
# GraphQL with Django Graphene

Using Graphene requires lazy loading ie. hacks with Getup. Essentially URL patterns are registered dynamically by Getup middleware indirection. Graphene behaves as Django is already configured straight from import like Django models and admin site do. Apart from this Graphene example works the same.

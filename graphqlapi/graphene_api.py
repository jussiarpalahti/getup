
from django.urls import path
import graphene
from graphene_django.views import GraphQLView

class Query(graphene.ObjectType):
    hello = graphene.String(argument=graphene.String(default_value="stranger"))

    def resolve_hello(self, info, argument):
        return "Hello " + argument

schema = graphene.Schema(query=Query)

urlpatterns = (path("", GraphQLView.as_view(graphiql=True, schema=schema)),)

from django.urls import path, include
from django.http import HttpResponse

def urls():
    return (path('hih', lambda x: HttpResponse('hih')), path("graphql", include("graphqlapi.api")),)


import getup.conf

CONF = getup.conf.Configuration(
    INSTALLED_APPS=["graphqlapi", "graphene_django"]
    + getup.conf.Configuration().INSTALLED_APPS,
    MIDDLEWARE=["getup.middleware.getup_middleware"]
    + getup.conf.Configuration().MIDDLEWARE,
    DATABASES={"default": {"ENGINE": "django.db.backends.sqlite3", "NAME": "app.db"}},
    getup_urls=urls,
)
CONF.configure()

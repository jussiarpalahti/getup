from django.urls import path, include, re_path
from django.http import HttpResponse
from django.template import RequestContext, Template


TEMPLATE = """
This is our msg: {{ msg }}
"""


def f(request):
    template = Template(TEMPLATE)
    context = RequestContext(request, {"msg": "hih"})
    return HttpResponse(template.render(context))


def urls():
    return (
        path("jee", f),
        path("fuu", lambda x: HttpResponse("fuu")),
        path("", include('server.app')),
    )


import getup.conf

CONF = getup.conf.Configuration(
    getup_urls=urls,
    INSTALLED_APPS=["sample", "server"] + getup.conf.Configuration().INSTALLED_APPS,
    MIDDLEWARE=["getup.middleware.getup_middleware"]
    + getup.conf.Configuration().MIDDLEWARE,
)
CONF.configure()


from django.urls import path
from django.http import HttpResponse

CONF = {
  "INSTALLED_APPS": ["serverless"],
  "ROOT_URLCONF": (path("", lambda x: HttpResponse("no server")),),
  "DEBUG": True,
  "SECRET_KEY": "randobrando",
}

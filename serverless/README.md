
# Serverless a.k.a one function only Django

Most minimal Django config. Url pattern is directly the view function. Options for secret_key, installed_apps and debug are kind of redundant. Django requires its boilerplate and it pays to be explicit.

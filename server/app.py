from django.urls import re_path, path
from django.http import FileResponse, HttpResponseBadRequest
from django.conf import settings
import pathlib


URL = getattr(settings, "SERVER_URL", settings.MEDIA_URL)
if URL.startswith('/'):
    URL = URL.split("/", 1)[1]

ROOT = pathlib.Path(getattr(settings, "SERVER_ROOT", settings.MEDIA_ROOT))


def server_view(request, path, root_path=ROOT):

    request_path = root_path.joinpath(path).absolute()
    if root_path in request_path.parents and request_path.is_file():
        return FileResponse(open(str(request_path), "rb"))
    else:
        return HttpResponseBadRequest("Invalid request")


urlpatterns = (re_path(f"{URL}(.*)", server_view),)

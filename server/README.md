
# File server app

Simple view function to serve media or other files from within Django. Uses FileResponse which should be speedy enough.

Same pattern can be used for permission requiring files, but this app has no knowledge of such things.

Uses `MEDIA_ROOT` and `MEDIA_URL` if its own settings `SERVER_URL` and `SERVER_ROOT` aren't provided.

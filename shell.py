
# 
# Console attaching shell command from Python
# For interactive use, containers, ssh, &c
# https://stackoverflow.com/questions/2651874/embed-bash-in-python
# 

def run_script(script, stdin=None):
    """Returns (stdout, stderr), raises error on non-zero return code"""
    import subprocess, sys
    # Note: by using a list here (['bash', ...]) you avoid quoting issues, as the 
    # arguments are passed in exactly this order (spaces, quotes, and newlines won't
    # cause problems):
    proc = subprocess.Popen(['bash', '-c', script])
        # stdout=subprocess.PIPE, stderr=subprocess.PIPE,
        # stdin=subprocess.PIPE)
    stdout, stderr = proc.communicate()
    if proc.returncode:
        print(proc.returncode, stdout, stderr, script)
        sys.exit()
    print("out")
    return stdout, stderr

class ScriptException(Exception):
    def __init__(self, returncode, stdout, stderr, script):
        self.returncode = returncode
        self.stdout = stdout
        self.stderr = stderr
        

if __name__ == "__main__":
  run_script("docker run -it --rm ubuntu /bin/bash")
  print("bye!")

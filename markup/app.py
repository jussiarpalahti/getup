import pathlib
import markdown
from django.urls import path
from django.http import HttpResponse
from django.template import RequestContext, Template
from django.conf import settings
from django.http import Http404
import os.path

DOC = """Title:   This is my title
Date:    January 1, 2019

# I am a doc

This is our msg: {{ msg }}

Your title is: {{ meta.title }}

## What about lists

 - mark
 - down
 - with lists

<ul>
{% for item in items %}
  <li>{{ item }}</li>
{% endfor %}
</ul>

<pre>
  Pure HTML here
</pre>
"""

TEMPLATE = """
{% extends "base.html" %}
{% block content %}
%%ARG%%
{% endblock %}
"""


def convert_md(doc=DOC):
    """
  Takes DOC and returns it as dict with HTML converted text
  and what metadata was found in doc
  """
    md = markdown.Markdown(extensions=["meta"])
    doc = md.convert(doc)
    return (doc, {k: " ".join(v) for k, v in md.Meta.items()})


def render_md(doc, request, resp):

    doc, meta = convert_md(doc)

    resp.update(dict(meta=meta))

    context = RequestContext(request, resp)

    tmpl = Template(TEMPLATE)

    return HttpResponse(tmpl.render(context).replace("%%ARG%%", doc))


def mdj(doc, request, resp):

    doc, meta = convert_md(doc)

    resp.update(dict(meta=meta))

    context = RequestContext(request, resp)

    tmpl = Template(TEMPLATE.replace("%%ARG%%", doc))

    return HttpResponse(tmpl.render(context))


def view(request, path):
    """
  Markdown + Django rendering view
  Takes doc path and returns rendered document
  """
    if hasattr(settings, "MDJ_DOCUMENT_ROOT"):
        root = pathlib.Path(settings["MDJ_DOCUMENT_ROOT"]).absolute()
    else:
        root = pathlib.Path(".").absolute()
    doc_path = root.joinpath(pathlib.Path(path).absolute())

    if not str(doc_path).startswith(str(root)):
        return HttpResponse("bad path")

    resp = {"items": [1, 2, 3]}

    if not os.path.exists(doc_path):
        raise Http404

    if doc_path.suffix == ".md":
        return render_md(doc_path.read_text(), request, resp)
    elif doc_path.suffix == ".mdj":
        return mdj(doc_path.read_text(), request, resp)
    else:
        return HttpResponse("nope")


urlpatterns = (path("<str:path>", view),)

import pathlib
import attr
import getup.conf


class MarkupConfiguration(getup.conf.Configuration):
    MDJ_DOCUMENT_ROOT: str = str(pathlib.Path(__file__).resolve().parent)

CONF = MarkupConfiguration(
    ROOT_URLCONF=urlpatterns,
    INSTALLED_APPS=["markup"] + getup.conf.Configuration().INSTALLED_APPS,
)


from django.urls import path, include
from django.http import HttpResponse
from django.template import RequestContext, Template


TEMPLATE = """
This is our msg: {{ msg }}
"""


def f(request):
    template = Template(TEMPLATE)
    context = RequestContext(request, {"msg": "hih"})
    return HttpResponse(template.render(context))


extra_urls = [path("direct", lambda x: HttpResponse("hih")), path("included/", include('extras'))]

urlpatterns = (path("jee", f), path("fuu", lambda x: HttpResponse("fuu")))

import getup.conf

CONF = getup.conf.Configuration(
    ROOT_URLCONF=urlpatterns,
    MIDDLEWARE=["getup.middleware.getup_middleware"] + getup.conf.Configuration().MIDDLEWARE,
    INSTALLED_APPS=["invert"] + getup.conf.Configuration().INSTALLED_APPS,
    DATABASES={"default": {"ENGINE": "django.db.backends.sqlite3", "NAME": "app.db"}},
    extras=dict(extra_settings="extra_value"),
    getup_urls=extra_urls
)
CONF.configure()

from django.urls import path
from django.http import HttpResponse
from django.template import RequestContext, Template


TEMPLATE = """
This is our msg: {{ msg }}
"""


def f(request):
    template = Template(TEMPLATE)
    context = RequestContext(request, {"msg": "hih"})
    return HttpResponse(template.render(context))


urlpatterns = (path("jee", f), path("fuu", lambda x: HttpResponse("fuu")))


import getup.conf
CONF = getup.conf.Configuration()
CONF.combine(
    getup.conf.Configuration(ROOT_URLCONF=urlpatterns, INSTALLED_APPS=["konf"])
)

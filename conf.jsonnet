local basic = {
  INSTALLED_APPS: [
    'django.contrib.admin.apps.SimpleAdminConfig',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
  ],
  TEMPLATES: {
    django: {
      BACKEND: 'django.template.backends.django.DjangoTemplates',
      DIRS: [],
      APP_DIRS: true,
      OPTIONS: {
        context_processors: [
          'django.template.context_processors.debug',
          'django.template.context_processors.request',
          'django.contrib.auth.context_processors.auth',
          'django.contrib.messages.context_processors.messages',
        ],
      },
    },
  },
  MIDDLEWARE: [
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
  ]
  };
{
  conf(urls, apps, allowed_hosts, debug, secret_key,templates=[], middleware=[]):: {
    ROOT_URLCONF: urls,
    ALLOWED_HOSTS: allowed_hosts,
    DEBUG: true,
    SECRET_KEY: secret_key,
    INSTALLED_APPS: apps,
    TEMPLATES: [basic.TEMPLATES.django] + templates,
    MIDDLEWARE: middleware + basic.MIDDLEWARE, // TODO: Middleware ordering?
    STATIC_URL: '/static/'
  }
}
